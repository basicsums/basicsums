---
title: 'Basicsums: A Python package for computing structural sums and the effective conductivity of random composites'
tags:
  - Python
  - basic sums
  - structural sums
  - computational materials science
  - pattern recognition
  - data science
authors:
  - name: Wojciech Nawalaniec
    orcid: 0000-0002-5475-9173
    affiliation: "1,2"
affiliations:
 - name: Faculty of Mathematics, Physics and Technical Science, Pedagogical University of Cracow, Poland
   index: 1
 - name: Materialica+ Research Group (www.materialica.plus)
   index: 2
date: 27 January 2019
bibliography: paper.bib
---


# Structural sums

Modern computational theory of random composites [@GluMitNaw:2018] is based on *Structural sums* (also known as *basic sums*) introduced for 2D composites
with circular non-overlapping inclusions. They  perform the same role in description of microstructure as the $n$-point correlation functions [@GluMitNaw:2018]. This leads to
the precise geometrical description of heterogeneous
media.  Considered sums constitute an integral part of the effective conductivity (EC) formula of random composites modelled by distributions of non-overlapping disks on the plane (in torus topology). For example of obtaining symbolic-numerical formula for the EC of random materials using a Monte Carlo method, see @CzaMitNaw:2012. For more theory and applications, see @GluMitNaw:2018. 

Besides the original application, structural sums are considered to form a set of geometric features of data represented by points or disks on the plane [@wnaw:2019]. This yields the application of the sums as a tool of analysis and visualization of different kind of data. For example, @MitNaw:2015 used structural sums in the systematic investigation of dynamically changing microstructures. Other applications cover, for instance,  analysis of the collective behaviour of bacteria [@MitCza:2017], a description of random non-overlapping walks of disks on the plane [@wnawChapter:2015], and parameterized comparison of composite materials obtained in different technological processes [@KurRyl:2017]. 
The recent research [@wnaw:2019] brings the definition of the *structural sums feature vector* and clearly shows that the vector is a carrier of information. Construction of such feature vector space enables the immediate application of machine learning tools and data analysis techniques to data represented by points or disks on the plane, especially to random structures.

For the precise definition of structural sums, the EC, and the underlying geometry, see @GluMitNaw:2018 or [documentation](https://basicsums.bitbucket.io/theory/bs_theory.html).


The `basicsums` module has contributed to a number of research results, e.g. @wnaw:2019, @wnaw:2017. It was also used in several projects of the Materialica+ Research Group, presented in @GluMitNaw:2018.

# ``Basicsusms`` package description


`Basicsusms` is a Python 3 package for computing structural sums and the EC of random composites. The package forms a set of tools that can be divided into following submodules presented in Fig. 1. For theoretical details refer to the [Theory overview](https://basicsums.bitbucket.io/theory.html) in documentation.

![Modules dependency and data flow in the `basicsusm` package.](package_picture.pdf)

The key component of the `basicsums` package is the `basicsums.basic_sums` module, providing the `BasicSums` class tackling calculations of structural sums being convolutions of elliptic functions. The underlying elliptic functions and lattice sums are implemented in `basicsums.weierstrass` and `basicsums.eisenstein` modules. The `basicsums.multi_indexes` module incorporates procedures returning sets of multi-indexes of structural sums that are sufficient to achieve a given polynomial approximation of the effective conductivity or a certain approximation of the structural sums feature vector. For more details, see @wnaw:2019 and [documentation](https://basicsums.bitbucket.io/theory/geometric_parameters.html#approximation-of-mathcal-m-e-and-symbolic-precision).
The `basicsums.conductivity` module is designed for an original application of basic sums: computing both purely symbolic and symbolic-numeric formulae for the EC of random 2D composites with circular non-overlapping inclusions. The `basicsums.preparation` unit provides functions that help the user to prepare data for the application of structural sums. In order to visualize a system of disks in a given two-periodic cell the one-function `basicsums.show_disks`  module can be used.



# Algorithms

``Basicsums`` provides an object oriented architecture strongly relying on recent results on algorithms and methods for calculations of structural sums and the EC of random composites [@wnaw:2016; @wnaw:2017]. For instance, the paper @wnaw:2017 describes in detail algorithms and methods for efficient computations of *discrete multidimensional convolutions of functions* with applications to structural sums. The application of the specific vector-matrix representation of a convolution, presented therein, significantly reduces computational complexity of calculations and allows reusing intermediate results cached during the computations.

# Conclusions

Since structural sums may seem to be computationally and conceptually complex, the main goal of this package is to provide a high level of abstraction in calculations, hiding the underlying theory and algorithms. The ready-to-use package may be helpful for scientists from the field of computational materials science.

The software may also attract the attention of researchers from other fields in order to experiment with structural sums as data analysis tools. Since structural sums have proven their value as carriers of geometrical information, the package can be applied to other types of data as long as the data is represented by distributions of non-overlapping disks, as wellas points, on the plane. The area of potential applications involves, for instance, plane objects frequently studied in biological and medical images.


# References
