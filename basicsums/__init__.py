'''
A package for computing structural sums and the effective \
conductivity of random composites


Structural sums (also known as basic sums) are mathematical \
objects originating from the computational materials science, \
considered to form a set of geometric features of data \
represented by points or disks on the plane.

See the structural sums overview, package description, \
tutorials, example usage and API reference on \
http://basicsums.bitbucket.io
'''

name = 'basicsums'
